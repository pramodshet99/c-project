#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include "Phase-I.h"

date_t start_date__;
date_t end_date__;

int isgreater(date_t* start_date, date_t  *end_date) {
	if (end_date->yy_ < start_date->yy_) {
		
		return 0;
	}
	else if ((end_date->yy_ == start_date->yy_) && (end_date->mm_ < start_date->mm_))
		return 0;
	else if ((end_date->yy_ == start_date->yy_) && (end_date->mm_ == start_date->mm_) && (end_date->dd_ < start_date->dd_))
		return 0;
	else
	{	
	return 1;}
}

int isLeap(int year) {
	return (((year %4 == 0) && (year%100 != 0)) || year%400 == 0);
}

int valid_date(date_t *date) {
	
	if(date->mm_ < 1 || date->mm_>12)
		return 0;
	if (date->dd_ < 1|| date->dd_ > 31)
		return 0;
	
	if(date->mm_ == 2) {
		if (isLeap(date->yy_))
			return (date->dd_ <= 29);
		else
			return (date->dd_ <= 28);
	}
		
	if(date->mm_ == 4 || date->mm_ == 6 || date->mm_ == 9||date->mm_ == 11)
		return (date->dd_ <= 30);
	
	return 1;

}

void initialize(date_t *start_date, date_t *end_date) 
{
	//Read start date and end date from user and also check for valid date.
	printf("Enter start date in formate dd-mm-yyyy\n");
	scanf("%d-%d-%d", &start_date->dd_, &start_date->mm_, &start_date->yy_);
	
	printf("Enter end date in formate dd-mm-yyyy\n");
	scanf("%d-%d-%d", &end_date->dd_, &end_date->mm_, &end_date->yy_);
	
	
	
	if(valid_date(start_date)) {
		if (valid_date(end_date) && (isgreater(start_date,end_date)))
		{
			
			start_date__.dd_ = start_date->dd_;
			start_date__.mm_ = start_date->mm_;
			start_date__.yy_ = start_date->yy_;
			end_date__.dd_ = end_date->dd_;
			end_date__.mm_ = end_date->mm_;
			end_date__.yy_ = end_date->yy_;
		}
		else {
			printf("Invalid Date\n");
		}
	}
	else  {
			printf("Invalid Date\n");
	}

}


void next_billing_dates(date_t start_date, date_t end_date, date_t* next_date)
{
	
	start_date.dd_ += 1;
	if(valid_date)
	{
			next_date->dd_=start_date.dd_;
			next_date->mm_=start_date.mm_;
			next_date->yy_=start_date.yy_;
	}
	else
	{
		start_date.dd_=1;
		start_date.mm_+=1;
	
		
	if(valid_date)
	{
			next_date->dd_=start_date.dd_;
			next_date->mm_=start_date.mm_;
			next_date->yy_=start_date.yy_;
	
	}
	else
	{
		start_date.yy_+=1;
		start_date.mm_=1;
		next_date->dd_=start_date.dd_;
		next_date->mm_=start_date.mm_;
		next_date->yy_=start_date.yy_;
	}
	}
	/*
	It will generate next date between start date and end date 
	inclusive. 
*/
}



void generate_bills()
{
	int n;
	float amount;
	int i;
	FILE *customer;
	FILE *bills;
	customer = fopen("customers.txt","r");
	bills = fopen("generate_bill.txt","a");
	//Next Date
	date_t next_date; 
			next_date.dd_=start_date__.dd_;
			next_date.mm_=start_date__.mm_;
			next_date.yy_=start_date__.yy_;
	while(isgreater(&next_date,&end_date__))
	{
		n = (rand()%(40-20+1))+20;
		
		char name[100];
		i = 0;
	
		while(i < n)
		{
			if(feof(customer))
				rewind(customer);
			fgets(name, 20, customer);
			i++;

			amount = (rand() % (5000-100+1)) + 100;
		name[strcspn(name,"\n")]=0;
		
		fprintf(bills,"%d-%d-%d:%s:%f\n",next_date.dd_,next_date.mm_,next_date.yy_,name,amount); 
		}
	
	
	
	next_billing_dates(next_date, end_date__,&next_date);
	}
	fclose(bills);
	fclose(customer);
/*
	Input is taken from "customers.txt"
	initialize and billing_dates functions are called.
	It generates the amount randomly - Rs and paise. (100 <= amount <= 5000). 
	and also the bill structure as <Date>:name of customer:total amount
	output stored in file - "generate_bill.txt"
*/
}
void day_wise_total()
{
	FILE *bills;
	FILE *days;
	char *ptr;
	char next_day[100]={"0"};
	char date[100];
	days=fopen("day_wise.txt","a");
	bills = fopen("generate_bill.txt","r");
	while(!feof(bills))
	{
	fscanf(bills,"%s",date);
	
	ptr=strtok(date,":");
	
	if(strcmp(next_day,date))
	{
	fprintf(days,"%s","\n\n");
	strcpy(next_day,date);
	fprintf(days,"%s\n",ptr);
	}
	
	
	
	ptr=strtok(NULL,":");
	if(ptr!=NULL)
		fprintf(days,"%s:",ptr);

	ptr=strtok(NULL,":");
	if(ptr!=NULL)
		fprintf(days,"%s\n",ptr);

	//fgets(date,100,bills);
	
	//fprintf(days,"%s",date);
	//fscanf(bills,"%s",date);
	
	
	}
	fclose(days);
	fclose(bills);
	
/*
	store day wise bill in "day_wise.txt".
	20 <= bill_count <= 40.
	customer name is different for same day.
*/
}
void customer_wise_total()
{
	
	FILE *bills;
	FILE *days;
	FILE *customers;
	char *ptr;
	char next_name[100]={"0"};
	char day[100];
	char name[100]={"0"};
	char date[100];
	char amount[100];
	
	days=fopen("customer_wise.txt","a");
	
	customers = fopen("customers.txt","r");
	while(!feof(customers))
	{
		bills = fopen("generate_bill.txt","r");
		fgets(name, 50, customers);
		
		name[strcspn(name,"\n")]=0;
		fprintf(days,"%s\n",name);
		while(!feof(bills))
		{
		fscanf(bills,"%s",day);
		
		ptr=strtok(day,":");
		if(ptr!=NULL)
		strcpy(date,ptr);
		ptr=strtok(NULL,":");
	
		if(ptr!=NULL)
		strcpy(next_name,ptr);
	
		ptr=strtok(NULL,":");
		if(ptr!=NULL)
		strcpy(amount,ptr);
		
		if(strcmp(next_name,name)==0 && ptr!=NULL)
		{
		
		fprintf(days,"%s:",date);
		fprintf(days,"%s\n",amount);
		}
	
	}
	rewind(bills);
	}
	fclose(bills);
	fclose(days);
	fclose(customers);
	
	
/*
	store customer wise bill in "customer_wise.txt".
*/
}
