#ifndef PHASEI
#define PHASEI

/*
	Input file : "customers.txt".
				this file contains the 50 unique customer's name.
	output file :"generate_bill.txt".			
				this file contain bill details in the below given formate 
				Bill-formate : <Date>:name of customer:total amount
				
				:"day_wise.txt".
				this file contain bill details according to the corresponding days.
				:"customer_wise.txt"
				this file contain bill details according to the corresponding customer details.
*/

struct date {
	int dd_;
	int mm_;
	int yy_;
};

typedef struct date date_t;



void initialize(date_t *start_date, date_t *end_date);
/*
	Read start date and end date from user and also check for valid date.
*/


void next_billing_dates(date_t start_date, date_t end_date, date_t* next_date);
/*
	It will generate valid dates between start date and end date 
	inclusive, and the next date will be updated in next_date .
*/



void generate_bills(); 
/*
	read Input from "customers.txt"
	initialize and next_billing_dates functions are called.
	It generates the amount randomly - Rs and paise. (100 <= amount <= 5000). 
	output stored in file - "generate_bill.txt"
*/

void day_wise_total();
/*
	read input from  "generate_bill.txt"
	store day wise bill in "day_wise.txt".
	20 <= bill_count <= 40.
	customer name is different for same day.
*/

void customer_wise_total();
/*	
	read input from  "generate_bill.txt"
	store customer wise bill in "customer_wise.txt".
	customer name is different for same day.
*/



#endif